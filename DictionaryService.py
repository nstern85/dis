import logging
import os

import binary_search
import rootpath


class DictionaryService:
    def __init__(self, path="dictionary.txt"):
        path = os.path.join(rootpath.detect(), path)
        self.logger = logging.getLogger('app')
        try:
            with open(path, "r") as fp:
                self.words = [line[:-1] for line in fp]
                self.words.sort()
        except FileNotFoundError:
            self.words = []
            self.logger.error(f"could not find file {path} !!!")

    def get_matching_words(self, prefix: str) -> list:
        res = []
        if self.words:
            idx = binary_search.search(self.words, prefix)
            while self.words[idx].startswith(prefix):
                res.append(self.words[idx])
                idx += 1
        return res

    def len(self) -> int:
        return len(self.words)

