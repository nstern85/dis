# Dictionary service

this project using redis and flask, with docker compose

# Getting Started

These instructions will get you started - having a executable project

### Installing

add a file named .env to the solution
this file will hold your environment variables:
```
REDIS_HOST=redis
REDIS_PORT=6379
REDIS_PASSWORD=1234567
```
after you finish adding this file run the following line
```
docker-compose up --build
```
after the project has been built you can use 

```
docker-compose up
```

## Built With
* binary-search==0.3.0
* flask==1.1.1
* redis==3.3.11
* python-dotenv==0.10.3
* mock==3.0.5
* rootpath==0.1.1

##API
you'll need to use port 9000
```
GET http://0.0.0.0:9000/dictionary/AA
```
and the response will be: 
```
[
  "AA", 
  "AAH", 
  "AAHED", 
  "AAHING", 
  "AAHS", 
  "AAL", 
  "AALII", 
  "AALIIS", 
  "AALS", 
  "AARDVARK", 
  "AARDVARKS", 
  "AARDWOLF", 
  "AARDWOLVES", 
  "AARGH", 
  "AARRGH", 
  "AARRGHH", 
  "AAS", 
  "AASVOGEL", 
  "AASVOGELS"
]
```
for statistics
```
GET http://0.0.0.0:9000/statistics:
```
and the response would be:
```
{
  "averageRequestHandleTimeMs": 0.2789497375488281, 
  "requestHandledCount": 8, 
  "wordCount": 204833
}
```
