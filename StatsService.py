import logging
from statistics import mean

from redis import Redis

from DictionaryService import DictionaryService
from utils.consts import REQUESTS_COUNTER, REQUESTS_DURATIONS


class StatsService:
    def __init__(self, redis_connection: Redis, dictionary_service: DictionaryService):
        self.logger = logging.getLogger("app")
        self.dictionary_service = dictionary_service
        self.redis = redis_connection

    def _get_requests_counter(self):
        requests_counter = self.redis.get(REQUESTS_COUNTER)
        if not requests_counter:
            return 0
        else:
            return int(requests_counter)

    def get_stats(self):
        word_count = self.dictionary_service.len()
        num_of_requests = self._get_requests_counter()
        duration_avg_ms_binary = self.redis.lrange(REQUESTS_DURATIONS, 0, -1)
        duration_avg_ms = None
        if duration_avg_ms_binary:
            duration_avg_ms = mean([float(b) for b in duration_avg_ms_binary])
        return {
            "wordCount": word_count,
            "requestHandledCount": num_of_requests,
            "averageRequestHandleTimeMs": duration_avg_ms
        }

