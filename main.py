import os
import redis
from flask import Flask, jsonify
from DictionaryService import DictionaryService
from dotenv import load_dotenv

from StatsService import StatsService
from utils.request_timer import request_timer

load_dotenv()
app = Flask(__name__)
dictionary_service = DictionaryService()
r = redis.Redis(host=os.environ.get("REDIS_HOST"), port=os.environ.get("REDIS_PORT"),
                password=os.environ.get("REDIS_PASSWORD"), decode_responses=True)
statistics_service = StatsService(r, dictionary_service)


@app.route('/dictionary/<prefix>')
@request_timer
def dictionary(prefix):
    return jsonify(dictionary_service.get_matching_words(prefix))


@app.route('/statistics')
def statistics():
    return jsonify(statistics_service.get_stats())


@app.route('/')
def health_check():
    resp = jsonify(success=True)
    return resp


if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True, port=9000)




