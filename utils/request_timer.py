import os
import time
from functools import wraps
from redis import Redis

from utils.consts import REQUESTS_DURATIONS, REQUESTS_COUNTER

r = Redis(host=os.environ.get("REDIS_HOST"), port=os.environ.get("REDIS_PORT"),
          password=os.environ.get("REDIS_PASSWORD"))


def request_timer(method):
    @wraps(method)
    def wrapper(*args, **kwds):
        start_time = time.time()
        val = method(*args, **kwds)
        duration_ms = (time.time() - start_time) * 1000
        r.lpush(REQUESTS_DURATIONS, duration_ms)
        r.incr(REQUESTS_COUNTER)
        r.ltrim(REQUESTS_DURATIONS, 0, 1000)
        return val
    return wrapper
