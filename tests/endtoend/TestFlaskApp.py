import unittest

from main import app


class TestFlaskApp(unittest.TestCase):
    def setUp(self) -> None:
        app.config['TESTING'] = True
        app.config['DEBUG'] = True
        self.app = app.test_client()

    def test_dictionary(self):
        response = self.app.get("dictionary/AA")
        self.assertEqual(response.status, '200 OK')

    def test_statistics(self):
        response = self.app.get("/statistics")
        self.assertEqual(response.status, '200 OK')


if __name__ == '__main__':
    unittest.main()
