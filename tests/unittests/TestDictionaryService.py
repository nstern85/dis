import os
import unittest

import rootpath as rootpath

from DictionaryService import DictionaryService


class TestDictionaryService(unittest.TestCase):

    def setUp(self) -> None:
        root_path = rootpath.detect()
        path = os.path.join(root_path, "dictionary.txt")
        self.dictionary_service = DictionaryService(path)

    def test_empty_dictionary_service(self):
        ds = DictionaryService("../aaa.txt")
        self.assertEqual(ds.len(), 0)

    def test_get_matching_words_not_exist(self):
        self.assertListEqual(self.dictionary_service.get_matching_words("123"), [])

    def test_get_matching_words_exact_match(self):
        matching_lst = ['AA', 'AAH', 'AAHED', 'AAHING', 'AAHS', 'AAL', 'AALII', 'AALIIS', 'AALS', 'AARDVARK',
                        'AARDVARKS', 'AARDWOLF', 'AARDWOLVES', 'AARGH', 'AARRGH', 'AARRGHH', 'AAS', 'AASVOGEL',
                        'AASVOGELS']
        self.assertListEqual(self.dictionary_service.get_matching_words("AA"), matching_lst)

    def test_len(self):
        root_path = rootpath.detect()
        with open(os.path.join(root_path, "dictionary_less_than_6.txt"), "r") as fp:
            length = len(list(fp))
        ds = DictionaryService(os.path.join(root_path, "dictionary_less_than_6.txt"))
        self.assertEqual(ds.len(), length)


if __name__ == '__main__':
    unittest.main()
