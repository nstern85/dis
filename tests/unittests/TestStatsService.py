import random
import unittest

import mock

from StatsService import StatsService


def mocking_get(_):
    if random.randint(0, 1):
        return str(random.randint(0, 200))


def mocking_len():
    return random.randint(0, 14600)


def mocking_lrange(key_name, start, end):
    if random.randint(0, 1):
        return []
    arr_len = random.randint(1, 100)
    return [str(random.uniform(0.5, 5.0))] * arr_len


class TestStatsService(unittest.TestCase):
    def setUp(self) -> None:
        dictionary_service_mock = mock.Mock()
        redis_mock = mock.Mock()

        def func():
            return str(random.randint(0, 200))
        dictionary_service_mock.len = func
        self.stats_service = StatsService(redis_mock, dictionary_service_mock)

    def test_get_requests_counter(self):
        with mock.patch.object(self.stats_service.redis, 'get', side_effect=mocking_get):
            with mock.patch.object(self.stats_service.dictionary_service, 'len', side_effect=mocking_len):
                with mock.patch.object(self.stats_service.redis, "lrange", side_effect=mocking_lrange):
                    self.assertTrue(isinstance(self.stats_service._get_requests_counter(), int))

    def test_get_stats_word_count(self):
        with mock.patch.object(self.stats_service.redis, 'get', side_effect=mocking_get):
            with mock.patch.object(self.stats_service.dictionary_service, 'len', side_effect=mocking_len):
                with mock.patch.object(self.stats_service.redis, "lrange", side_effect=mocking_lrange):
                    stats = self.stats_service.get_stats()
                    self.assertTrue(isinstance(stats["wordCount"], int))

    def test_get_stats_request_handled_count(self):
        with mock.patch.object(self.stats_service.redis, 'get', side_effect=mocking_get):
            with mock.patch.object(self.stats_service.dictionary_service, 'len', side_effect=mocking_len):
                with mock.patch.object(self.stats_service.redis, "lrange", side_effect=mocking_lrange):
                    stats = self.stats_service.get_stats()
                    self.assertTrue(isinstance(stats["requestHandledCount"], int))

    def test_average_request_handle_time_ms(self):
        with mock.patch.object(self.stats_service.redis, 'get', side_effect=mocking_get):
            with mock.patch.object(self.stats_service.dictionary_service, 'len', side_effect=mocking_len):
                with mock.patch.object(self.stats_service.redis, "lrange", side_effect=mocking_lrange):
                    stats = self.stats_service.get_stats()
                    self.assertTrue(isinstance(stats["averageRequestHandleTimeMs"], float) or
                                    stats["averageRequestHandleTimeMs"] is None)


if __name__ == '__main__':
    unittest.main()
